import logging

from flask import current_app
from flask import session
from flask_script import Manager
# 迁移，Migrate将app跟db进行关联
from flask_migrate import Migrate, MigrateCommand
from info import create_app, db, models


# 通过指定配置名字添加对应的app
# create_app类似与工厂方法
app = create_app('development')
# 将manager跟app进行关联
manager = Manager(app)
# 将app跟db关联，先传app后传db
Migrate(app, db)
# 将迁移命令添加到manager中,命令的名字取db
manager.add_command('db', MigrateCommand)




if __name__ == '__main__':
    manager.run()

# manage只做程序入口,只关心启动所需东西