

# session可以存到多个地方比较常见的存到redis里面，也可以存到mysql里面
import logging
from redis import StrictRedis


class Config(object):
    DEBUG = True
    # 随机生成一个加密session的数
    SECRET_KEY = "i/sOdpoJB70QAy8F9zs1Sg3np4Sr0nnO3mhFJh012s9JoIhO8vHt30IUkf3kdoPW"
    # 为数据库添加配置
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information27"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 在请求结束时，如果指定配置为True，那么SQLAlchemy会自动执行一次db.session.commit()操作
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    # redis的配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    # session保存位置配置，session保存在redis里
    SESSION_TYPE = 'redis'
    # 开启session签名
    SESSION_USE_SIGNER = True
    # 指定session保存的redis
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)
    # 设置需要过期
    SESSION_PERMANENT = False
    # 设置过期时间
    PERMANENT_SESSION_LIFETIME = 8640 * 2

    LOG_LEVEL = logging.DEBUG

class DevelopmentConfig(Config):
    """开发环境下的配置"""
    DEBUG = True

class ProductionConfig(Config):
    """生产环境下的配置"""
    DEBUG = False
    LOG_LEVEL = logging.WARNING

class TestingConfig(Config):
    """单元环境下的配置"""
    DEBUG = True
    """只有开启testing被测试的代码出现问题时才会定位到被测试的一行"""
    TESTING = True

config = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "testing": TestingConfig
}